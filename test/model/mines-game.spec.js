import Difficulty from "../../src/model/difficulty";
import GameState from "../../src/model/game-state";
import MinesGame from "../../src/model/mines-game";
import Tile, { BLANK, MINE } from "../../src/model/tile";
import TileRandomizer from "../../src/model/tile-randomizer";

describe('MinesGame', () => {

    it('is defined', () => {
        expect(MinesGame).toBeDefined();
    });

    describe('constructor()', () => {

        it('retains settings provided', () => {
            let game = new MinesGame(new TileRandomizer(), new Difficulty('TEST', 4, 3, 5));

            expect(game.width).toEqual(4);
            expect(game.height).toEqual(3);
            expect(game.mines).toEqual(5);

            expect(game.tiles.length).toEqual(12);
            expect(game.mineLocations.length).toEqual(5);

            // Ensure the tile neighbour relationships are built
            expect(game.tiles[5].neighbours.length).toEqual(8);
        });

    });

    describe('reveal()', () => {

        /** @type {MinesGame} */
        let game = null;

        beforeEach(() => {
            game = new MinesGame(new TileRandomizer(), new Difficulty('TEST', 4, 3, 5));
        });

        it('only reveals the tile specified if it is blank and touches any mine', () => {
            let blankTileTouchingMine = game.tiles.find(t => t.type === BLANK && t.neighbours.find(n => n.type === MINE));

            let revealedTilesBefore = getRevealedTilesCount(game);
            expect(blankTileTouchingMine).toBeDefined();

            // Ensure revealing a blank tile keeps the game active
            expect(game.reveal(blankTileTouchingMine)).toEqual(GameState.ACTIVE);

            // Ensure the tile has been marked as revealed
            expect(blankTileTouchingMine.revealed).toBe(true);

            // Ensure only 1 tile is revealed
            let revealedTilesAfter = getRevealedTilesCount(game);
            expect(revealedTilesAfter - revealedTilesBefore).toEqual(1);
        });

        it('changes the game state to LOST if a mine is revealed', () => {
            let mineIndex = game.mineLocations[0];
            let mineTile = game.tiles[mineIndex];

            expect(game.reveal(mineTile)).toEqual(GameState.LOST);
        });

        it('does not reveal a flagged tile', () => {
            let flaggedTile = game.tiles[0];
            flaggedTile.flagged = true;

            // Ensure the game remains active when this happens
            expect(game.reveal(flaggedTile)).toEqual(GameState.ACTIVE);

            // Ensure the flagged tile is not revealed
            expect(flaggedTile.revealed).toBe(false);
        });

        it('reveals neighbours when the tile has no adjacent mines', () => {
            // Generate a game with faked random map with all mines stacked at the end:
            // 0 0 0 1
            // 2 3 4 M
            // M M M M
            game = new MinesGame(new FakeTileRandomizer(), new Difficulty('TEST', 4, 3, 5));

            // Reveal the top left tile should expand the first 6 tiles
            expect(game.reveal(game.tiles[0])).toEqual(GameState.ACTIVE);

            // We should now see:
            // 0 0 0 1
            // 2 3 ? ?
            // ? ? ? ?
            let revealedTileIndices = game.tiles.filter(t => t.revealed).map((t, i) => i);
            expect(revealedTileIndices).toEqual([0, 1, 2, 3, 4, 5]);
        });

    });

    describe('toggleFlag()', () => {

        /** @type {MinesGame} */
        let game = null;

        beforeEach(() => {
            game = new MinesGame(new TileRandomizer(), new Difficulty('TEST', 4, 3, 5));
        });

        it('does not flag a revealed tile', () => {
            let revealedTile = game.tiles[0];
            revealedTile.revealed = true;
            revealedTile.flagged = false;

            expect(game.toggleFlag(revealedTile)).toEqual(GameState.ACTIVE);
            expect(revealedTile.flagged).toBe(false);
        });

        it('changes the game state to WON if all mines have been flagged', () => {
            game = new MinesGame(new FakeTileRandomizer(), new Difficulty('TEST', 5, 5, 1));
            
            let theOnlyMine = game.tiles[24];
            expect(game.toggleFlag(theOnlyMine)).toEqual(GameState.WON);
        });

        it('changes the game state to ACTIVE_WITH_ERRORS when right number of flags are placed but they are not correct', () => {
            game = new MinesGame(new FakeTileRandomizer(), new Difficulty('TEST', 5, 5, 1));
            
            let theWrongTile = game.tiles[23];
            expect(game.toggleFlag(theWrongTile)).toEqual(GameState.ACTIVE_WITH_ERRORS);
        });

        it('updates the flag locations when a tile is flagged', () => {
            let tileToFlag = game.tiles[0];

            expect(game.toggleFlag(tileToFlag)).toEqual(GameState.ACTIVE);
            expect(tileToFlag.flagged).toBe(true);
            expect(game.flagLocations).toEqual([0]);
        });

        it('updates the flag locations when a tile is unflagged', () => {
            let flaggedTile = game.tiles[0];
            flaggedTile.flagged = true;
            game.flagLocations = [0];
            
            expect(game.toggleFlag(flaggedTile)).toEqual(GameState.ACTIVE);
            expect(flaggedTile.flagged).toBe(false);
            expect(game.flagLocations).toEqual([]);
        });


        it('will not decrease mines remaining below 0', () => {
            // Place 1 mine on the board, then flag two tiles and ensure the mines remaining count stays at 0
            game = new MinesGame(new FakeTileRandomizer(), new Difficulty('TEST', 5, 5, 1));

            game.toggleFlag(game.tiles[0]);
            expect(game.minesRemaining).toEqual(0);
            
            game.toggleFlag(game.tiles[1]);
            expect(game.minesRemaining).toEqual(0);
        });

    });


});

/**
 * Counts the number of tiles that are revealed.
 * @param {MinesGame} game 
 */
function getRevealedTilesCount(game) {
    return game.tiles.filter(t => t.revealed).length;
}

/**
 * A fake tile randomizer that stacks all mines at the latter end
 * of the board.
 */
class FakeTileRandomizer extends TileRandomizer {
    constructor() {
        super();
    }

    generate(width, height, mines) {
        let count = width * height;
        let tiles = new Array(count).fill();

        for (let i = 0; i < tiles.length; i++) {
            tiles[i] = new Tile(i, (i < count - mines) ? BLANK : MINE);
        }

        return tiles;
    }
}