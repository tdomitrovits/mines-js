import Tile, { BLANK, MINE } from '../../src/model/tile';

describe('Tile', () => {

    it('is defined', () => {
        expect(Tile).toBeDefined();
    });

    describe('constructor()', () => {

        it('builds an instance correctly', () => {
            let t = new Tile(123, 'TEST');

            expect(t.id).toEqual(123);
            expect(t.type).toEqual('TEST');
            expect(t.revealed).toBe(false);
            expect(t.flagged).toBe(false);
        });

    });


    describe('mines()', () => {

        /** @type {Tile} */
        let t = null;

        beforeEach(() => {
            t = new Tile(1, BLANK);
        });
        
        it('counts mines on neighbouring tiles', () => {
            // Add 8 neighbours - 5 mines and 3 blanks
            [2, 3, 4, 5, 6].forEach(n => t.neighbours.push(new Tile(n, MINE)));
            [7, 8, 9].forEach(n => t.neighbours.push(new Tile(n, BLANK)));

            expect(t.mines()).toEqual(5);
        });

        it('uses previously calculated value to avoid recalculation', () => {
            t.mineCount = 3;

            expect(t.mines()).toEqual(3);
        });

    });

    describe('toggleFlag()', () => {

        /** @type {Tile} */
        let t = null;

        beforeEach(() => {
            t = new Tile(1, BLANK);
        });

        it('flags an unflagged tile', () => {
            t.flagged = false;

            expect(t.toggleFlag()).toBe(true);
            expect(t.flagged).toBe(true);
        });

        it('unflags a flagged tile', () => {
            t.flagged = true;

            expect(t.toggleFlag()).toBe(false);
            expect(t.flagged).toBe(false);
        });

    });

});