# Mines JS
Mines JS is a project built for demonstrative purposes only and was written by [Tyson Domitrovits](mailto:tdomitrovits@hotmail.com). It is a simple Javascript implementation of the game [Microsoft Minesweeper](https://en.wikipedia.org/wiki/Microsoft_Minesweeper). 

Icons used are from [Font Awesome](https://fontawesome.com/)'s free icon offering.

## Live Demo
The latest version can be found here: https://mines-js.netlify.com.

## Running Locally

### Building Output
Execute `npm run build` to build the deployable artifacts in the dist folder.

### Running the Development Server
Execute `npm run dev` to launch a web browser with hot reloading for development.

### Generating Code Coverage Report
Execute `npm run coverage` to run the test suite and generate a code coverage report.

### Running the Tests in Watch Mode
Execute `npm run test` to run tests in watch mode for test development.

## TODO List

### Tech Debt List
- Add tests now that the structure has stabilized
- Organize code consistently (header/main view template and CSS extracted from the root template file and stylesheet)
- Remove SVG markup embedded in JS

### Improvements
- Track and display best time records for each difficulty
- Make the entire UI fluid to allow more customization of grid dimensions and mobile friendliness
- Enable the bitbucket pipeline to guard merges to master branch against failed CI builds
- New colourscheme
- Restarting game does not need to refresh the page