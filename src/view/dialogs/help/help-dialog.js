import Dialog from "../dialog";
import template from './help-dialog.html';
import './help-dialog.css';

/**
 * A dialog implementation for the help documentation
 */
export default class HelpDialog extends Dialog {
    
    constructor() {
        super();
    }

    /**
     * @override
     * @returns {string}
     */
    html() {
        return template;
    }
    
}