import Dialog from "../dialog";
import template from './end-game-dialog.html';

/**
 * A dialog implementation for the end of game outcome.
 */
export default class EndGameDialog extends Dialog {
    
    /**
     * @param {string} title 
     * @param {string} message 
     */
    constructor(title, message) {
        super();
        this.title = title;
        this.message = message;
    }

    /**
     * @override
     * @returns {string}
     */
    html() {
        return template.replace(`{{ title }}`, this.title).replace(`{{ message }}`, this.message);
    }
    
}