import './dialog.css';

/**
 * A base dialog concept that is only responsible for building a template.
 */
export default class Dialog {

    /**
     * Builds the HTML output of the dialog.
     * 
     * @returns {string}
     */
    html() {
        throw `Unsupported Operation: html(). This method must be implemented by a subclass.`;
    }

}