import './game-board-view.css';

const TILE_WIDTH = 64;

/**
 * The game board view
 */
export default class GameBoardView {

    /**
     * Creates the view for the game board.
     * @param {HTMLElement} element 
     */
    constructor(element) {
        this.element = element;

        this.width = 0;
        this.height = 0;
        this.tiles = [];
    }

    /**
     * Reset any flagged or revealed tiles to a hidden state.
     */
    hideAll() {
        // Reset the revealed tiles
        this.element.querySelectorAll('.tile').forEach(tile => {
            tile.innerHTML = ``;
            tile.classList.remove('tile--flagged');
            tile.classList.remove('tile--revealed');
        });
    }

    /**
     * Update the game board to match the dimensions of the game board.
     * 
     * @param {number} width 
     * @param {number} height 
     */
    refresh(width, height) {
        // Update the width and height values
        this.width = width;
        this.height = height;
        
        this.element.style.gridTemplateColumns = `repeat(${width}, 1fr)`;
        this.element.style.gridTemplateRows = `repeat(${height}, 1fr)`;
        this.element.style.width = `${width * TILE_WIDTH}px`;

        // Clear the old child nodes
        this.element.childNodes.forEach(child => child.remove());

        // Fill with new child nodes
        this.tiles = new Array(width * height).fill().map((t, index) => {
            let tile = document.createElement('div');

            tile.className = 'tile';
            tile.setAttribute('data-tile-id', index);
            this.element.append(tile);

            return tile;
        });

    }

    /**
     * @param {number} index - The index of the tile to reveal
     * @param {number} count - The number of mines touching the tile to be displayed
     */
    reveal(index, count) {
        let tile = this.tiles[index];

        tile.classList.add('tile--revealed');
        tile.innerHTML = count > 0 ? count : ``;
    }

    /**
     * @param {number} index - The index of the tile to show a mine
     */
    showMine(index) {
        let tile = this.tiles[index];

        tile.classList.add('tile--revealed');
        tile.innerHTML = `<span class="mine"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="bomb" class="svg-inline--fa fa-bomb fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M440.5 88.5l-52 52L415 167c9.4 9.4 9.4 24.6 0 33.9l-17.4 17.4c11.8 26.1 18.4 55.1 18.4 85.6 0 114.9-93.1 208-208 208S0 418.9 0 304 93.1 96 208 96c30.5 0 59.5 6.6 85.6 18.4L311 97c9.4-9.4 24.6-9.4 33.9 0l26.5 26.5 52-52 17.1 17zM500 60h-24c-6.6 0-12 5.4-12 12s5.4 12 12 12h24c6.6 0 12-5.4 12-12s-5.4-12-12-12zM440 0c-6.6 0-12 5.4-12 12v24c0 6.6 5.4 12 12 12s12-5.4 12-12V12c0-6.6-5.4-12-12-12zm33.9 55l17-17c4.7-4.7 4.7-12.3 0-17-4.7-4.7-12.3-4.7-17 0l-17 17c-4.7 4.7-4.7 12.3 0 17 4.8 4.7 12.4 4.7 17 0zm-67.8 0c4.7 4.7 12.3 4.7 17 0 4.7-4.7 4.7-12.3 0-17l-17-17c-4.7-4.7-12.3-4.7-17 0-4.7 4.7-4.7 12.3 0 17l17 17zm67.8 34c-4.7-4.7-12.3-4.7-17 0-4.7 4.7-4.7 12.3 0 17l17 17c4.7 4.7 12.3 4.7 17 0 4.7-4.7 4.7-12.3 0-17l-17-17zM112 272c0-35.3 28.7-64 64-64 8.8 0 16-7.2 16-16s-7.2-16-16-16c-52.9 0-96 43.1-96 96 0 8.8 7.2 16 16 16s16-7.2 16-16z"></path></svg></span>`;
    }
    
    /**
     * Toggles the flag icon over the specified tile.
     * 
     * @param {number} index - The index of the tile
     * @param {boolean} isFlagged - Whether or not the tile is flagged
     */
    toggleFlag(index, isFlagged) {
        let tile = this.tiles[index];

        if (isFlagged) {
            tile.classList.add('tile--flagged');
            tile.innerHTML = `<span class="flag"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="flag-checkered" class="svg-inline--fa fa-flag-checkered fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M243.2 189.9V258c26.1 5.9 49.3 15.6 73.6 22.3v-68.2c-26-5.8-49.4-15.5-73.6-22.2zm223.3-123c-34.3 15.9-76.5 31.9-117 31.9C296 98.8 251.7 64 184.3 64c-25 0-47.3 4.4-68 12 2.8-7.3 4.1-15.2 3.6-23.6C118.1 24 94.8 1.2 66.3 0 34.3-1.3 8 24.3 8 56c0 19 9.5 35.8 24 45.9V488c0 13.3 10.7 24 24 24h16c13.3 0 24-10.7 24-24v-94.4c28.3-12.1 63.6-22.1 114.4-22.1 53.6 0 97.8 34.8 165.2 34.8 48.2 0 86.7-16.3 122.5-40.9 8.7-6 13.8-15.8 13.8-26.4V95.9c.1-23.3-24.2-38.8-45.4-29zM169.6 325.5c-25.8 2.7-50 8.2-73.6 16.6v-70.5c26.2-9.3 47.5-15 73.6-17.4zM464 191c-23.6 9.8-46.3 19.5-73.6 23.9V286c24.8-3.4 51.4-11.8 73.6-26v70.5c-25.1 16.1-48.5 24.7-73.6 27.1V286c-27 3.7-47.9 1.5-73.6-5.6v67.4c-23.9-7.4-47.3-16.7-73.6-21.3V258c-19.7-4.4-40.8-6.8-73.6-3.8v-70c-22.4 3.1-44.6 10.2-73.6 20.9v-70.5c33.2-12.2 50.1-19.8 73.6-22v71.6c27-3.7 48.4-1.3 73.6 5.7v-67.4c23.7 7.4 47.2 16.7 73.6 21.3v68.4c23.7 5.3 47.6 6.9 73.6 2.7V143c27-4.8 52.3-13.6 73.6-22.5z"></path></svg></span>`;
        } else {
            tile.classList.remove('tile--flagged');
            tile.innerHTML = ``;
        }
    }

}