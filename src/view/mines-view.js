import EndGameDialog from './dialogs/end-game/end-game-dialog';
import GameBoardView from './board/game-board-view';
import HeaderView from './header-view';
import HelpDialog from './dialogs/help/help-dialog';

/**
 * The main game view
 */
export default class MinesView {

    /**
     * @param {HTMLElement} root 
     */
    constructor(root) {
        this.root = root;

        this.header = new HeaderView(root.querySelector('.mines-js-header'));
        this.board = new GameBoardView(root.querySelector('.mines-js-board'));
        this.showHelpButton = root.querySelector('.mines-js-show-help');
        this.warningElement = root.querySelector('.mines-js-warning');
        this.helpDialog = new HelpDialog();
    }
    
    /**
     * @param {string}
     */
    set warning(message) {
        this.warningElement.innerHTML = message;
        if (message.length > 0) {
            document.body.classList.add('mines-js-warning-state');
        } else {
            document.body.classList.remove('mines-js-warning-state');
        }
    }

    /**
     * @param {number}
     */
    set minesRemaining(amount) {
        this.header.remainingMines = amount;
    }

    /**
     * @param {number[]} indices 
     */
    revealMines(indices) {
        indices.forEach(i => this.board.showMine(i));
    }

    /**
     * @param {number} index
     * @param {boolean} isFlagged 
     */
    flagTile(index, isFlagged) {
        this.board.toggleFlag(index, isFlagged);
    }

    /**
     * @param {string} title 
     * @param {string} message 
     */
    showEndOfGameDialog(title, message) {
        // Clear any active dialogs
        this.hideDialogs();

        // Attach the dialog and substitute title and message into the template
        document.body.innerHTML += new EndGameDialog(title, message).html();
    }

    /**
     * Display the help dialog
     */
    showHelpDialog() {
        // Clear any active dialogs
        this.hideDialogs();

        // Attach the help dialog
        document.body.innerHTML += this.helpDialog.html();
    }

    /**
     * Hide all overlays and dialogs that may be open
     */
    hideDialogs() {
        document.body.querySelectorAll(`.mines-js-dialog-overlay`).forEach(o => o.remove());
        document.body.querySelectorAll(`.mines-js-dialog`).forEach(n => n.remove());
    }

}