/**
 * The heads up display view
 */
export default class HeaderView {

    /**
     * @param {HTMLElement} element 
     */
    constructor(element) {
        this.element = element;

        this.minesElement = this.element.querySelector('.mines-js-remaining-value');
        this.difficultyElement = this.element.querySelector('.mines-js-difficulty-value');
        this.elapsedTimeElement = this.element.querySelector('.mines-js-elapsed-time-value');
    }

    /**
     * @param {number} - The estimated number of mines remaining
     */
    set remainingMines(amount) {
        this.minesElement.innerHTML = `${amount}`;
    }

    /**
     * @param {number} - Elapsed time in seconds
     */
    set elapsedTime(time) {
        this.elapsedTimeElement.innerHTML = new Date(time * 1000).toISOString().substr(11, 8);
    }

    /**
     * @param {string} - The name of the difficulty
     */
    set difficulty(difficulty) {
        this.difficultyElement.value = difficulty;
    }
}
