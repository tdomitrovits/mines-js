import MinesController from './controller/mines-controller.js';

const gameElement = document.querySelector('.mines-js');

// Bootstrap the controller
new MinesController(gameElement);