/** 
 * Tile Module
 */
export const BLANK = 'blank';
export const MINE = 'mine';

export default class Tile {
    
    /**
     * @param {number} id 
     * @param {string} type
     */
    constructor(id, type) {
        this.id = id;
        this.type = type;
        this.neighbours = [];
        this.revealed = false;
        this.flagged = false;

        this.mineCount = null;
    }

    /**
     * @returns {number} the number of mines touching the tile.
     */
    mines() {
        if (this.mineCount === null) {
            this.mineCount = this.neighbours
                .map(n => n.type === MINE ? 1 : 0)
                .reduce((total, n) => total += n);
        }

        return this.mineCount;
    }

    /**
     * @returns {boolean} whether or not the tile is now flagged after being toggled.
     */
    toggleFlag() {
        this.flagged = !this.flagged;
        return this.flagged;
    }

}