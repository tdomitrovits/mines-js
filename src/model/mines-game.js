import Difficulty from "./difficulty";
import GameState from "./game-state";
import Tile, { BLANK, MINE } from "./tile";
import TileRandomizer from "./tile-randomizer";

export default class MinesGame {

    /**
     * Builds a new Mines game instance.
     * 
     * @param {TileRandomizer} tileRandomizer
     * @param {Difficulty} difficulty
     */
    constructor(tileRandomizer, difficulty) {
        this.tileRandomizer = tileRandomizer;
        this.width = difficulty.width;
        this.height = difficulty.height;
        this.mines = difficulty.mines;

        this.minesRemaining = this.mines;
        this.tiles = this.tileRandomizer.generate(this.width, this.height, this.mines);
        this.mineLocations = this.tiles.filter(t => t.type === MINE).map(t => t.id);
        this.flagLocations = [];

        // Create the neighbouring tile links
        this.tiles.forEach((tile, index) => {
            // West neighbour
            if (index % this.width > 0 && this.tiles[index - 1]) {
                tile.neighbours.push(this.tiles[index - 1]);
            }

            // East neighbour
            if (index % this.width < this.width - 1 && this.tiles[index + 1]) {
                tile.neighbours.push(this.tiles[index + 1]);
            }

            // Northwest neighbour
            if (index % this.width > 0 && this.tiles[index - this.width - 1]) {
                tile.neighbours.push(this.tiles[index - this.width - 1]);
            }
            
            // North neighbour
            if (this.tiles[index - this.width]) {
                tile.neighbours.push(this.tiles[index - this.width]);
            }
            
            // Northeast neighbour
            if (index % this.width < this.width - 1 && this.tiles[index - this.width + 1]) {
                tile.neighbours.push(this.tiles[index - this.width + 1]);
            }

            // Southwest neighbour
            if (index % this.width > 0 && this.tiles[index + this.width - 1]) {
                tile.neighbours.push(this.tiles[index + this.width - 1]);
            }

            // South neighbour
            if (this.tiles[index + this.width]) {
                tile.neighbours.push(this.tiles[index + this.width]);
            }

            // Southeast neighbour
            if (index % this.width < this.width - 1 && this.tiles[index + this.width + 1]) {
                tile.neighbours.push(this.tiles[index + this.width + 1]);
            }
        });
    }

    /**
     * Reveals the tile at the specified location and any additional tiles surrounding it if it has no 
     * mines in proximity.
     * 
     * @param {Tile} tile 
     * @returns {GameState} The game state after the tile has been revealed.
     */
    reveal(tile) {
        let resultingState = null;

        if (tile.flagged) {
            return GameState.ACTIVE;
        }
        
        tile.revealed = true;

        if (tile.type === MINE) {
            resultingState = GameState.LOST;
        } else {
            resultingState = GameState.ACTIVE;
        }

        if (tile.mines() === 0) {
            tile.neighbours
                .filter(n => !n.revealed)
                .forEach(t => this.reveal(t));
        }

        return resultingState;
    }

    /**
     * Toggles the flag for the provided tile.
     * 
     * @param {Tile} tile 
     * @returns {GameState} The game state after the tile's flag has been toggled.
     */
    toggleFlag(tile) {
        let resultingState = null;

        if (tile.revealed) {
            return GameState.ACTIVE;
        }

        // Track the flag locations
        if (tile.toggleFlag()) {
            this.flagLocations.push(tile.id);
        } else {
            this.flagLocations = this.flagLocations.filter(l => l !== tile.id);
        }

        let delta = this.mines - this.flagLocations.length;
        this.minesRemaining = delta >= 0 ? delta : 0;

        // Check for the win condition
        if (this.minesRemaining === 0) {

            let flaggedIndices = this.flagLocations.sort().join(',');
            let mineIndices = this.mineLocations.sort().join(',');

            if (flaggedIndices === mineIndices) {
                resultingState = GameState.WON;
            } else {
                resultingState = GameState.ACTIVE_WITH_ERRORS;
            }
        } else {
            resultingState = GameState.ACTIVE;
        }

        return resultingState;
    }

}