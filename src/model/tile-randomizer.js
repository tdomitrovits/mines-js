import Tile, { BLANK, MINE } from "./tile";

/**
 * A helper class to help initialize a new game board.
 */
export default class TileRandomizer {

    /**
     * Generates a new set of tiles for a gameboard of the provided dimensions and with
     * the number of mines listed scattered randomly throughout.
     * 
     * @param {number} width
     * @param {number} height
     * @param {number} mines
     * @returns {Tile[]}
     */
    generate(width, height, mines) {

        let tiles = new Array(width * height).fill().map((tile, index) => new Tile(index, BLANK));
        
        // Build an array of all the possible locations for mines
        let tileIndices = tiles.map((t, index) => index);

        // Perform a randomized shuffle on the tile indices to allow us to choose
        // random, valid and distinct locations for the mines to be placed
        for (let i = tileIndices.length - 1; i > 0; i--) {
            let randomIndex = Math.round(Math.random() * (tileIndices.length - 1));
            let tempValue = tileIndices[i];
            tileIndices[i] = tileIndices[randomIndex];
            tileIndices[randomIndex] = tempValue;
        }

        // Place the mines from the list of shuffled locations
        for (let i = 0; i < mines; i++) {
            tiles[tileIndices[i]] = new Tile(tileIndices[i], MINE);
        }

        // Return the updated tile array
        return tiles;
    }
}