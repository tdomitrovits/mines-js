/**
 * Game States that the game nagivates through.
 */

const GameState = {
    NEW: 'new',
    ACTIVE: 'active',
    ACTIVE_WITH_ERRORS: 'active_with_errors',
    LOST: 'lost',
    WON: 'won'
};

export default GameState;