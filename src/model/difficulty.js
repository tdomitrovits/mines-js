/**
 * The Difficulty model that represents the settings of a difficulty - the board
 * dimensions and the number of mines to generate.
 */
export default class Difficulty {

    /**
     * @param {string} name 
     * @param {number} width 
     * @param {number} height 
     * @param {number} mines 
     */
    constructor(name, width, height, mines) {
        this.name = name;
        this.width = width;
        this.height = height;
        this.mines = mines;
    }
    
};
