import Difficulty from "../model/difficulty";
import GameState from "../model/game-state";
import MinesView from "../view/mines-view";
import MinesGame from "../model/mines-game";
import TileRandomizer from "../model/tile-randomizer";

// Difficulty Settings
const NEWBIE = new Difficulty('Newbie', 12, 6, 5);
const EASY = new Difficulty('Easy', 16, 9, 15);
const NORMAL = new Difficulty('Normal', 16, 9, 30);
const HARD = new Difficulty('Hard', 16, 9, 40);
const DANGEROUS = new Difficulty('Dangerous', 16, 9, 50);

// List of difficulties
const DIFFICULTIES = [NEWBIE, EASY, NORMAL, HARD, DANGEROUS];

/**
 * The main controller of the game that processes input events, interacts with the model
 * and updates the views.
 */
export default class MinesController {

    /**
     * @param {HTMLElement} element 
     */
    constructor(element) {
        this.model;
        this.view = new MinesView(element);

        this.state;
        this.elapsedTime;

        DIFFICULTIES.map(diff => {
            let opt = document.createElement('option');
            opt.value = diff.name;
            opt.innerHTML = diff.name;
            return opt;
        }).forEach(opt => this.view.header.difficultyElement.append(opt));

        this.difficulty = DIFFICULTIES.find(diff => diff.name === window.localStorage.getItem('difficulty')) || NORMAL;
        this.view.header.difficulty = this.difficulty.name;

        this.init();

        this.view.header.difficultyElement.addEventListener('change', e => {
            window.localStorage.setItem('difficulty', this.view.header.difficultyElement.value);
            window.location = window.location;
        });

        this.view.showHelpButton.addEventListener('click', e => {
            this.view.showHelpDialog();
        });
    }

    /**
     * @param {Difficulty} [difficulty]
     */
    init(difficulty = this.difficulty) {

        // Get a new game instance
        this.model = new MinesGame(new TileRandomizer(), difficulty);
        this.state = GameState.NEW;
        this.elapsedTime = -1;

        this.lastTap = new Array(this.model.tiles.length);
        this.model.tiles.forEach(tile => {
            this.lastTap[tile.id] = 0;
        });
        
        let width = difficulty.width;
        let height = difficulty.height;
        let mines = difficulty.mines;
        
        // Refresh the board if the dimensions have changed since it was last refreshed
        if (this.view.board.width !== width || this.view.board.height !== height) {

            // Build the grid
            this.view.board.refresh(width, height);

            // Attach the event handlers
            this.view.board.tiles.forEach((t, index) => {

                // Set up the left click handler to reveal tiles
                t.addEventListener('click', e => {
                    e.preventDefault();
                    if (this.state === GameState.LOST || this.state === GameState.WON) {
                        return;
                    }
                    this.reveal(index);

                    if (this.elapsedTime === -1) {
                        this.elapsedTime = 0;
                        this.view.header.elapsedTime = this.elapsedTime;
                        this.timer = setInterval(() => { 
                            this.elapsedTime++;
                            this.view.header.elapsedTime = this.elapsedTime;
                        }, 1000);
                    }
                });
                
                // Set up the right click handler to flag tiles
                t.addEventListener('contextmenu', e => {
                    e.preventDefault();
                    if (this.state === GameState.LOST || this.state === GameState.WON) {
                        return;
                    }
                    this.toggleFlag(index);

                    if (this.elapsedTime === -1) {
                        this.elapsedTime = 0;
                        this.view.header.elapsedTime = this.elapsedTime;
                        this.timer = setInterval(() => { 
                            this.elapsedTime++;
                            this.view.header.elapsedTime = this.elapsedTime;
                        }, 1000);
                    }
                });


                // Set up touch screen events for mobile devices
                t.addEventListener('touchstart', e => {

                    // Prevent multi-touches from triggering a game effect since
                    // the intention may not be clear
                    if (e.touches.length > 1) {
                        return;
                    }

                    e.preventDefault();

                    let now = new Date().getTime();

                    if (now - this.lastTap[index] >= 400) {
                        setTimeout(() => {
                            if (this.state === GameState.LOST || this.state === GameState.WON) {
                                return;
                            }
                            this.toggleFlag(index);
                        }, 400);
                    } else {
                        if (this.state === GameState.LOST || this.state === GameState.WON) {
                            return;
                        }
                        this.reveal(index);
                    }
                    
                    this.lastTap[index] = now;
                });
            });
        }

        // Hide all tiles that were revealed
        this.view.board.hideAll();

        // Reset the warning and mine count
        this.view.warning = ``;
        this.view.minesRemaining = mines;
    }

    /**
     * Reveals the tile at the given index and expands any other tiles if necessary.
     * It may trigger the game the end in defeat.
     * 
     * @param {number} index 
     */
    reveal(index) {
        let tile = this.model.tiles[index];
        let state = this.model.reveal(tile);

        this.state = state;

        switch (state) {
            case GameState.ACTIVE:
                this.model.tiles.filter(tile => tile.revealed).forEach(tile => this.view.board.reveal(tile.id, tile.mineCount));
                break;
            case GameState.LOST:
                this.view.revealMines(this.model.mineLocations);
                this.view.showEndOfGameDialog(`Oh no!`, `You hit a mine and lost the game!`);
                break;
            default:
                throw `Reached unexpected state: '${state}' when revealing tile at index ${index}.`;
        }
    }

    /**
     * Toggles the flagged state of the tile at the given index and will update the
     * remaining mines count accordingly.
     * 
     * @param {number} index 
     */
    toggleFlag(index) {
        let tile = this.model.tiles[index];
        let state = this.model.toggleFlag(tile);

        this.state = state;

        if (tile.revealed) {
            return;
        }

        this.view.flagTile(index, tile.flagged);
        this.view.minesRemaining = this.model.minesRemaining;

        switch (state) {
            case GameState.ACTIVE:
                this.view.warning = ``;
                break;
            case GameState.ACTIVE_WITH_ERRORS:
                this.view.warning = `There are one or more incorrectly flagged mines.`;
                break;
            case GameState.WON:
                this.view.showEndOfGameDialog(`Congratulations!`, `You have correctly flagged all of the mines and have won the game!`);
                break;
            default:
                throw `Reached unexpected state: '${state}' when revealing tile at index ${index}.`;
        }
    }
}
